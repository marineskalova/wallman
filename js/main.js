$(document).ready(function(){

	/**
	 *		Open drop-down list
	 */

	$('.js-button-selection').on('click', function() {
		$(this).toggleClass('is-open');
	});

	/**
	 *		Open the catalog
	 */

	$('.js-open-catalog').on('click', function() {
		$('.header-panel__drop-down').toggleClass('is-visible');
	});

	/**
	 *		Close the previously opened drop-down lists
	 */

	$(document).on('click', function(e) {
		if (!$(e.target).closest('.js-button-selection').length) {
			$('.js-button-selection').removeClass('is-open');
		}

		if (!$(e.target).closest('.header-panel').length) {
			$('.header-panel__drop-down').removeClass('is-visible');
		}
		e.stopPropagation();
	});

	/**
	 *		Fix the menu
	 */

	$nav = $('.header-panel');
	$window = $(window);
	$h = $nav.offset().top;
	$(window).scroll(function(){
		if ($(window).scrollTop() > $h) {
			$nav.addClass('fixed');
		} else {
			$nav.removeClass('fixed');
		}
	});

	/**
	 *		Full-screen slider
	 */
	
	var $slickElement = $('.js-approach__list')
	
	$slickElement.on('init reInit beforeChange', function (event, slick, currentSlide, nextSlide) {
		var i = (nextSlide ? nextSlide : 0) + 1;
		$('.js-status').html(i + '<span class="all-pages">/' + slick.slideCount + '</span>');
	});
	
	$slickElement.slick({
		arrows: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		infinite: false,
		appendArrows: $('.approach__dots'),
		prevArrow: $('.js-approach-arrow__prev'),
		nextArrow: $('.js-approach-arrow__next')
	});
	
	$('.examples__list').slick({
		infinite: true,
		slidesToShow: 1,
		centerMode: true,
		variableWidth: true,
		draggable: false,
		appendArrows: $('.examples__dots'),
		prevArrow: $('.js-examples-arrow__prev'),
		nextArrow: $('.js-examples-arrow__next')
	});
	
	$('.js-reviews-text').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		initialSlide: 2,
		arrows: false,
		fade: true,
		asNavFor: '.js-reviews-icon'
	});
	
	$('.js-reviews-icon').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		initialSlide: 2,
		asNavFor: '.js-reviews-text',
		dots: false,
		arrows: false,
		centerMode: true,
		focusOnSelect: true,
		variableWidth: true,
		infinite: true,
	});
	
	$('.js-banner-list').slick({
		slidesToShow: 1,
		dots: false,
		arrows: false,
		fade: true,
		speed: 300,
		infinite: false,
	});

	$('.js-catalog-page-list__carousel-one').slick({
		arrows: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		speed: 300,
		infinite: false,
		dots: true,
		appendDots: $('.catalog-page__dots-one')
	});

	$('.js-catalog-page-list__carousel-two').slick({
		arrows: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		speed: 300,
		infinite: false,
		dots: true,
		appendDots: $('.catalog-page__dots-two')
	});

	$('.js-catalog-page-list__carousel-three').slick({
		arrows: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		speed: 300,
		infinite: false,
		dots: true,
		appendDots: $('.catalog-page__dots-three')
	});

	$('.js-catalog-page-list__carousel-four').slick({
		arrows: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		speed: 300,
		infinite: false,
		dots: true,
		appendDots: $('.catalog-page__dots-four')
	});

	$('.js-catalog-page-list__carousel-five').slick({
		arrows: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		speed: 300,
		infinite: false,
		dots: true,
		appendDots: $('.catalog-page__dots-five')
	});

	$('.js-catalog-page-list__carousel-six').slick({
		arrows: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		speed: 300,
		infinite: false,
		dots: true,
		appendDots: $('.catalog-page__dots-six')
	});

	$('.js-product-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.js-product-slider-nav'
	});

	$('.js-product-slider-nav').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		asNavFor: '.js-product-slider',
		dots: false,
		centerMode: false,
		focusOnSelect: true
	});

	/**
	 *		Switching banners	
	 */
	
	$('.js-banner-list').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
		$('.js-banner-buttom').removeClass('is-active');
		$('.js-banner-buttom').eq(nextSlide).addClass('is-active');
	});
	
	$('.js-banner-buttom').on('click', function() {
		var indexSlide = $(this).index();
		$('.js-banner-buttom').removeClass('is-active');
		$(this).addClass('is-active');
		$('.js-banner-list').slick('slickGoTo', indexSlide, false);
	});
	
	$('.js-select').select2({
		minimumResultsForSearch: Infinity,
		placeholder: 'Назначение перегородок'
	});

	function formatState (state) {
		if (!state.id) { return state.text; }
		var $state = $(
			'<span><img src="img/' + state.element.value + '.jpg" class="product-param__img" /> ' + state.text + '</span>'
		);
		return $state;
	};

	$(".js-slider-colors").select2({
		minimumResultsForSearch: Infinity,
		templateResult: formatState,
		templateSelection: formatState

	});

	/**
	 *		Support form
	 */
	function validateRegExp(regExp) {
		return function(value) {
			return regExp.test(value)
		}
	}

	function validateRequired(value) {
		return !!value;
	}

	function validateElement(data, target) {
		if (!data || !target) return false;

		var name = target.name;
		var value = target.value;
		var isValid = !data[name].some(function (el) {return el(value) === false;});
		var placeholder = placeholders[name]
		if (isValid) {
			$(target).addClass('valid');
			$(target).removeClass('invalid');
			$(target)[0].placeholder = placeholder.placeholder;
		} else {
			$(target).addClass('invalid');
			$(target).removeClass('valid');
			$(target)[0].placeholder = placeholder.error;
		}

		return isValid
	}

	function forceValidate(data, selector) {
		var key,
			value,
			isValid = true;

		for (key in data) {
			if (!validateElement(data, $('[name=' + key + ']' + selector)[0])) {
				isValid = false;
			}
		}

		return isValid;
	}

	var formData = {
		name: [
			validateRegExp(/^([а-я]|[А-Я]|[a-z]|[A-z]){2,40}$/),
		],
		phone: [
			validateRegExp(/^\d{3,12}$/)
		],
	}

	var placeholders = {
		name: {
			placeholder: '',
			error: 'Заполните поле'
		},
		phone: {
			placeholder: '',
			error: 'Заполните поле'
		}
	}

	var FORM_SELECTOR = '.form-validation'

	$(FORM_SELECTOR).blur(function(event) {
		validateElement(formData, event.target)
	})

	$('#send-support').click(function (event) {
		event.preventDefault();
		var isFormValid = forceValidate(formData, FORM_SELECTOR)
		if (isFormValid) {
			// TODO http request
			$(".support-form__massage").addClass('show');
		}
	})
	$('#send-contacts').click(function (event) {
		event.preventDefault();
		var isFormValid = forceValidate(formData, FORM_SELECTOR)
		if (isFormValid) {
			// TODO http request
			$(".contacts-form__massage").addClass('show');
		}
	})

		/* Popup windows */
	if ( $('.js-open-popup').length ) {
		$('.js-open-popup').magnificPopup();
	}


});

/**
 *		input-file
 */
function getName (str){
	if (str.lastIndexOf('\\')){
		var i = str.lastIndexOf('\\')+1;
	}
	else{
		var i = str.lastIndexOf('/')+1;
	}
	var filename = str.slice(i);
	var uploaded = document.getElementById("input-file__label");
	uploaded.innerHTML = filename;
}
/**
 *		map-office
 */
 function initMap() {

	if($('#js-map-office')[0]){

		google.maps.event.addDomListener(window, 'load', init);
		google.maps.event.addDomListener(window, "resize", init);

		var markerSize = { x: 70, y: 91 };

		google.maps.Marker.prototype.setLabel = function(label){
				this.label = new MarkerLabel({
					map: this.map,
					marker: this,
					text: label
				});
				this.label.bindTo('position', this, 'position');
		};

		var MarkerLabel = function(options) {
				this.setValues(options);
				this.span = document.createElement('span');
				this.span.className = 'marker_text';
				position: new google.maps.LatLng( 55.811355, 37.5202320)
		};

		MarkerLabel.prototype = $.extend(new google.maps.OverlayView(), {
				onAdd: function() {
						this.getPanes().overlayImage.appendChild(this.span);
						var self = this;
						this.listeners = [
						google.maps.event.addListener(this, 'position_changed', function() { self.draw();    })];
				},
				draw: function() {
						var text = String(this.get('text'));
						var position = this.getProjection().fromLatLngToDivPixel(this.get('position'));
						this.span.innerHTML = text;
						this.span.style.left = (position.x + 10) + 'px';
						this.span.style.top = (position.y - markerSize.y + 75) + 'px';
				}
		});

		function init() {
				var mapOptions = {
						zoom: 14,
						scrollwheel: false,
						center: new google.maps.LatLng( 55.811377, 37.508553)
				};
				var mapElement = document.getElementById('js-map-office');
				var map = new google.maps.Map(mapElement, mapOptions);
				var marker = new google.maps.Marker({
						position: new google.maps.LatLng( 55.811357, 37.5202320),
						map: map,
						// icon: 'img/marker.png',
						label: 'улица Часовая,<br/> 28 корпус 51'
				});
		}
	}
/**
 *		map-production
 */

	if($('#js-map-production')[0]){

		google.maps.event.addDomListener(window, 'load', init);
		google.maps.event.addDomListener(window, "resize", init);

		var markerSize = { x: 70, y: 91 };

		google.maps.Marker.prototype.setLabel = function(label){
				this.label = new MarkerLabel({
					map: this.map,
					marker: this,
					text: label
				});
				this.label.bindTo('position', this, 'position');
		};

		var MarkerLabel = function(options) {
				this.setValues(options);
				this.span = document.createElement('span');
				this.span.className = 'marker_text';
				position: new google.maps.LatLng( 55.8835455, 37.4269983)
		};

		MarkerLabel.prototype = $.extend(new google.maps.OverlayView(), {
				onAdd: function() {
						this.getPanes().overlayImage.appendChild(this.span);
						var self = this;
						this.listeners = [
						google.maps.event.addListener(this, 'position_changed', function() { self.draw();    })];
				},
				draw: function() {
						var text = String(this.get('text'));
						var position = this.getProjection().fromLatLngToDivPixel(this.get('position'));
						this.span.innerHTML = text;
						this.span.style.left = (position.x + 10) + 'px';
						this.span.style.top = (position.y - markerSize.y + 80) + 'px';
				}
		});

		function init() {
				var mapOptions = {
						zoom: 14,
						scrollwheel: false,
						center: new google.maps.LatLng( 55.8835455,37.4269983)
				};
				var mapElement = document.getElementById('js-map-production');
				var map = new google.maps.Map(mapElement, mapOptions);
				var marker = new google.maps.Marker({
						position: new google.maps.LatLng( 55.8835455, 37.4269983),
						map: map,
						// icon: 'img/marker.png',
						label: 'Куркинское шоссе, 4'
				});
		}
	}
 };

